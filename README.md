# VMware Virtual Machine

## Template

For allow each new VM to be allocated a unique IPv4:

```bash
echo -n > /etc/machine-id
rm /var/lib/dbus/machine-id
ln -s /etc/machine-id /var/lib/dbus/machine-id
```

## Using this module

```bash
export GITLAB_TOKEN="<gitlab_token>"
```

```hcl
terraform {
  required_version = "~> 1.3.6"

  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = "~> 2.2.0"
    }
  }
}

provider "vsphere" {
  user                 = var.vsphere_username
  password             = var.vsphere_password
  vsphere_server       = var.vsphere_server_ip
  allow_unverified_ssl = true
}

locals {
  template             = "ubuntu-20"
  name                 = "vmware-virtual-machine-test"
  cores                = 4
  memory               = 4096
  root_disk_size       = 50
  additional_disk_size = 100
}

####################################################################################################
# Virtual Machine
####################################################################################################

module "vm" {
  source = "./modules/vmware-virtual-machine"

  template       = local.template
  name           = local.name
  cores          = local.cores
  memory         = local.memory
  root_disk_size = local.root_disk_size
}

####################################################################################################
# Virtual Machine with additional disk
####################################################################################################

module "vm_with_additional_disk" {
  source = "./modules/vmware-virtual-machine"

  template             = local.template
  name                 = "${local.name}-with-disk"
  cores                = local.cores
  memory               = local.memory
  root_disk_size       = local.root_disk_size
  additional_disk_size = local.additional_disk_size
}

####################################################################################################
# VMware Tags
####################################################################################################

resource "vsphere_tag_category" "main" {
  name        = local.name
  cardinality = "SINGLE"
  description = "Managed by Terraform"

  associable_types = [
    "VirtualMachine",
  ]
}

resource "vsphere_tag" "main" {
  name        = local.name
  category_id = vsphere_tag_category.main.id
  description = "Managed by Terraform"
}

####################################################################################################
# Virtual Machine with Tags and additional disk
####################################################################################################

module "vm_with_tags_and_additional_disk" {
  source = "./modules/vmware-virtual-machine"

  template             = local.template
  name                 = "${local.name}-with-tags-disk"
  cores                = local.cores
  memory               = local.memory
  root_disk_size       = local.root_disk_size
  additional_disk_size = local.additional_disk_size
  tags                 = [vsphere_tag.main.id]
}
```
