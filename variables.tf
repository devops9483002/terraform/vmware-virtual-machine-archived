variable "vsphere_datacenter" {
  type    = string
  default = "Datacenter"
}

variable "vsphere_datastore" {
  type = string
}

variable "vsphere_compute_cluster" {
  type    = string
  default = "cluster-01"
}

variable "vsphere_network" {
  type    = string
  default = "VM Network"
}

variable "vsphere_folder" {
  type        = string
  description = "The folder to create the VM in. i.e. /example-folder"
  default     = null
}

variable "name" {
  type = string
}

variable "linux_domain" {
  type        = string
  default     = "local"
  description = "The domain name for the Linux VM. i.e. local"
}

variable "cores" {
  type = number

  validation {
    condition     = var.cores >= 1 && var.cores <= 12
    error_message = "The number of cores must be between 1 and 12."
  }
}

variable "memory" {
  type = number

  validation {
    condition     = var.memory >= 1024 && var.memory <= 65536
    error_message = "The memory must be between 1024 and 65536."
  }
}

variable "root_disk_size" {
  type    = number
  default = 25

  validation {
    condition     = var.root_disk_size >= 25 && var.root_disk_size <= 10000
    error_message = "The disk size must be between 25 and 10000."
  }
}

variable "additional_disk_size" {
  type     = number
  default  = null
  nullable = true

  validation {
    condition     = var.additional_disk_size == null ? true : var.additional_disk_size >= 25 && var.additional_disk_size <= 10000
    error_message = "The disk size must be between 25 and 10000."
  }
}

variable "template" {
  type = string
}

variable "tags" {
  type     = list(string)
  default  = null
  nullable = true
}
