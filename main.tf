terraform {
  required_version = "~> 1.5"

  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = "~> 2.4.3"
    }
  }
}

data "vsphere_datacenter" "this" {
  name = var.vsphere_datacenter
}

data "vsphere_datastore" "this" {
  name          = var.vsphere_datastore
  datacenter_id = data.vsphere_datacenter.this.id
}

data "vsphere_compute_cluster" "this" {
  name          = var.vsphere_compute_cluster
  datacenter_id = data.vsphere_datacenter.this.id
}

data "vsphere_network" "this" {
  name          = var.vsphere_network
  datacenter_id = data.vsphere_datacenter.this.id
}

data "vsphere_virtual_machine" "template" {
  name          = var.template
  datacenter_id = data.vsphere_datacenter.this.id
}

resource "vsphere_virtual_machine" "this" {
  name             = var.name
  resource_pool_id = data.vsphere_compute_cluster.this.resource_pool_id
  datastore_id     = data.vsphere_datastore.this.id
  guest_id         = data.vsphere_virtual_machine.template.guest_id
  scsi_type        = data.vsphere_virtual_machine.template.scsi_type
  firmware         = "efi"
  num_cpus         = var.cores
  memory           = var.memory
  tags             = var.tags
  folder           = var.vsphere_folder

  network_interface {
    network_id = data.vsphere_network.this.id
  }

  disk {
    label            = "disk0"
    size             = var.root_disk_size
    unit_number      = 0
    thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
  }

  dynamic "disk" {
    for_each = var.additional_disk_size == null ? [] : [var.additional_disk_size]

    content {
      label            = "disk1"
      size             = var.additional_disk_size
      unit_number      = 1
      thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
    }
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id

    customize {
      linux_options {
        host_name = var.name
        domain    = var.linux_domain
      }

      network_interface {}
    }
  }
}
